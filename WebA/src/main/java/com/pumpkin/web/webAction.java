package com.pumpkin.web;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class webAction {
    private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        LOGGER.info("hello.....");
        String response = restTemplate.getForObject("http://SERVICE-A/user/1", String.class);
        return response;
    }
}
