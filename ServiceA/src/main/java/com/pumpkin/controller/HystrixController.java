package com.pumpkin.controller;

import com.pumpkin.dto.User;
import com.pumpkin.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/hystrix")
public class HystrixController {

    private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/get.do", method = RequestMethod.GET)
    public  User get(@RequestParam("id") String id){
        LOGGER.info("/get.do id=" +id);

        // 测试超时触发断路器
        try {
            int sleepTime = new Random().nextInt(100);
            LOGGER.info("sleepTime:" + sleepTime);
            Thread.sleep(sleepTime);
        } catch (Exception e){
            LOGGER.error(e);
        }

        return userService.get(Integer.valueOf(id));
    }


    @RequestMapping(value = "/batch.do", method = RequestMethod.GET)
    public List<User> batch(@RequestParam("ids") String ids){
        LOGGER.info("/batch.do=" +ids);
        return userService.get(ids.toString());

    }

    @RequestMapping(value = "/update.do", method = RequestMethod.PUT)
    public void update(@RequestParam("id") String id){
        LOGGER.info("/update.do=" +id);

    }

}
