package com.pumpkin.controller;


import com.pumpkin.dto.User;
import com.pumpkin.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User get(@PathVariable String id) {
        LOGGER.info("request id=" +id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userService.get(Integer.valueOf(id));
    }

    @RequestMapping(value = "/batch", method = RequestMethod.GET)
    public List<User> batch(@RequestBody String ids) {
        LOGGER.info("request ids=" +ids);
        return userService.get(ids);
    }


}
