package com.pumpkin.controller;

import com.pumpkin.dto.User;
import com.pumpkin.service.FeignService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignController implements FeignService {
    private final Logger LOGGER = Logger.getLogger(getClass());

    @Override
    public String hello(@RequestParam("name")String name) {
        return "hello";
    }

    @Override
    public User hello(@RequestParam("name")String name, @RequestParam("sex")Integer sex) {
        LOGGER.info("name=" +name+ " ,sex=" +sex);
        return new User("pumpkin", 0);
    }

    @Override
    public String hello(@RequestBody User user) {
        return "hello user=" +user.toString();
    }
}
