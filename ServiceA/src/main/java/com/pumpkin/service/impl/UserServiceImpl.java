package com.pumpkin.service.impl;

import com.pumpkin.dto.User;
import com.pumpkin.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User get(int id) {

        User user = new User();
        user.setId(id);
        user.setName("pumpkin");
        user.setSex(0);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }

    @Override
    public List<User> get(String ids) {
        List<User> userList = new ArrayList<User>();

        String[] idArr = ids.split(",");
        int idCount = idArr.length;

        for (int i = 0; i < idCount; i++){
            User user = new User();
            user.setId(Integer.valueOf(idArr[i]));
            user.setName("pumpkin");
            user.setSex(0);
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());

            userList.add(user);

        }

        return userList;
    }
}
