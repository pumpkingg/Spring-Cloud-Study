package com.pumpkin.service;

import com.pumpkin.dto.User;

import java.util.List;

public interface UserService {

    User get(int id);

    List<User> get(String ids);


}
