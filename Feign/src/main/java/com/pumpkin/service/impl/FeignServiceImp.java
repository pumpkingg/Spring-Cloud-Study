package com.pumpkin.service.impl;

import com.pumpkin.service.FeignService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "SERVICE-A")
public interface FeignServiceImp extends FeignService {
}
