package com.pumpkin.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pumpkin.dto.User;
import com.pumpkin.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feign")
public class FeignController {

    @Autowired
    private FeignService feignService;

    @RequestMapping(value = "/hello.do", method = RequestMethod.GET)
    public String feignHello() {
        StringBuilder sb = new StringBuilder();
        sb.append(feignService.hello("MIMI")).append("\n");
        sb.append(feignService.hello("MIMI", 20)).append("\n");
        sb.append(feignService.hello(new User("MIMI", 20))).append("\n");
        return sb.toString();
    }


}
