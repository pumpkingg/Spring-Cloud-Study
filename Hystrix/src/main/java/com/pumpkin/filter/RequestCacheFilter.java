package com.pumpkin.filter;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class RequestCacheFilter implements javax.servlet.Filter {

    private final Logger LOGGER = Logger.getLogger(getClass());

    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("初始化init filter...");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOGGER.info("执行filter");
        /**
         * 不初始化，会报如下错误
         * Request caching is not available. Maybe you need to
         * initialize the HystrixRequestContext
         * 初始化是在filter中进行（官方建议），但是每一次初始化之前的缓存就失效了，所以要测缓存，就只能在controller中调用两次，
         * 才能看到缓存的结果是否相同
         *  在同一用户请求的上下文中，相同依赖服务的返回数据始终保持一致  ---《spring cloud 微服务实战》有争论
         */
        HystrixRequestContext context = HystrixRequestContext.initializeContext();
        try {
            chain.doFilter(request, response);
        } finally {
            context.shutdown();
        }
    }
    public void destroy() {
        // TODO Auto-generated method stub
    }


}