package com.pumpkin.service;

import com.pumpkin.dto.User;
import java.util.List;
import java.util.concurrent.Future;

public interface HystrixServcie {

    User get(String id);
    Future<User> asyncGet(String id);
    List<User> batch(List<String> ids);
    void update(String id);

}
