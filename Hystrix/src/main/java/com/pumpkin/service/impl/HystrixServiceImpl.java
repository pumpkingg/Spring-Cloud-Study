package com.pumpkin.service.impl;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import com.pumpkin.dto.User;
import com.pumpkin.service.HystrixServcie;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.Future;

@Service
public class HystrixServiceImpl implements HystrixServcie {

    private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    RestTemplate restTemplate;

    @Override
    @HystrixCommand(fallbackMethod = "getFallBack", commandKey = "hystrixKey")
    @CacheResult
    public User get(String id) {
       // 模拟发生异常
/*        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return restTemplate.getForObject("http://SERVICE-A/hystrix/get.do?id=" +id, User.class);

    }

    @Override
    @CacheRemove(commandKey = "hystrixKey")
    @HystrixCommand
    public void update(String id) {
        restTemplate.put("http://SERVICE-A/hystrix/update.do?id=" +id, null);
    }

    private User getFallBack(String id, Throwable e){
        LOGGER.info("降级处理，e=" +e);
        return null;
    }



    @HystrixCollapser(batchMethod = "batch",
            collapserProperties =
                    {@HystrixProperty(name="timerDelayInMilliseconds", value = "100")})
    public Future<User> asyncGet(String id) {
        return null;
    }


    @Override
    @HystrixCommand
    public List<User> batch(List<String> ids) {

        if (ids == null){
            LOGGER.info("ids is null");
            return null;
        }
        LOGGER.info("当前线程：" +Thread.currentThread().getName());
        int idCount = ids.size();
        LOGGER.info("ids size=" +idCount);
        for (int i = 0; i < idCount; i++){
            LOGGER.info("ids[" +i+ "]=" +ids.get(i));
        }

        User[] user1 = restTemplate.getForObject("http://SERVICE-A/hystrix/batch.do?ids={1}", User[].class, StringUtils.join(ids, ","));
        LOGGER.info("res=" +user1.toString());
        return Arrays.asList(user1);
    }


}
