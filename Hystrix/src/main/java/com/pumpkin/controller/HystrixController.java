package com.pumpkin.controller;



import com.pumpkin.dto.User;
import com.pumpkin.service.HystrixServcie;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/hystrix")
public class HystrixController {

    private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    private HystrixServcie hystrixServcie;



    @RequestMapping("/get.do")
    public void get(@RequestParam("id") String id){
        LOGGER.info("第一次请求time=" +hystrixServcie.get(id).getCreateTime().getTime());
        LOGGER.info("第二次请求time="+ hystrixServcie.get(id).getCreateTime().getTime());

      //  hystrixServcie.update(id);//测试删除缓存

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        LOGGER.info("第二次请求time="+ hystrixServcie.get(id).getCreateTime().getTime());

    }

    @RequestMapping("/get1.do")
    public String get1(){
        try {
            Future<User> f1 = hystrixServcie.asyncGet(1+"");
            Future<User> f2 = hystrixServcie.asyncGet(2+"");


            Thread.sleep(300);

            Future<User> f3 = hystrixServcie.asyncGet(3+"");

            User user1 = f1.get();
            User user2 = f2.get();
            User user3 = f3.get();

            LOGGER.info("user1=" +user1.toString());
            LOGGER.info("user2=" +user2.toString());
            LOGGER.info("user3=" +user3.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return "hello";
    }


    @RequestMapping("/update.do")
    public void update(@RequestParam("id") String id){
         hystrixServcie.update(id);
    }


}
